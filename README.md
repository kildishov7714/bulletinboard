# BulletinBoard
The system of placing private ads.
main functionality:
a) Registration of the user in the system (user and administrator)
b) Editing the profile.
c) View the list of ads. Search and filtering.
d) The ability to add/edit/delete ads.
e) The ability to leave comments under ads.
f) Organization of personal correspondence between the buyer and the seller.
g) The ability to pay for the presence of an ad in the top of the issue.
h) A seller rating system that affects the position
of the seller's ads in the search results. The lower the rating, the lower the ad
in the output.
i) The user's sales history.

