package ru.kildishov.servise;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kildishov.exception.NotFoundException;
import ru.kildishov.model.Chat;
import ru.kildishov.model.Message;
import ru.kildishov.model.User;
import ru.kildishov.model.dto.MessageDTO;
import ru.kildishov.repository.*;

import java.util.List;
@Slf4j
@Service
public class MessageServise {


    private final IAdvertisementRepository advertisementRepository;
    private final IChatRepository chatRepository;
    private final ICommetnRepository commetnRepository;
    private final IMessageRepository messageRepository;
    private final IUserRepository userRepository;
    private  final  AdvertisementServise advertisementServise;
    private final  CommentService commentService;

    @Autowired
    public MessageServise(IAdvertisementRepository advertisementRepository, IChatRepository chatRepository, ICommetnRepository commetnRepository, IMessageRepository messageRepository, IUserRepository userRepository, AdvertisementServise advertisementServise, CommentService commentService) {

        this.advertisementRepository = advertisementRepository;
        this.chatRepository = chatRepository;
        this.commetnRepository = commetnRepository;
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
        this.advertisementServise = advertisementServise;
        this.commentService = commentService;
    }

    public MessageDTO toDTO(Message message){

        log.info("Start method toDTO with ", message);

        MessageDTO messageDTO = new MessageDTO();

        messageDTO.setId(message.getId());
        messageDTO.setText(message.getText());
        messageDTO.setIdChat(message.getChat().getId());
        messageDTO.setIdSender(message.getIdSender());

        return messageDTO;
    }

    public  Message fromDTO(MessageDTO messageDTO){
        log.info("Start method fromDTO with ", messageDTO);
        Message message = new Message();

        message.setId(messageDTO.getId());
        message.setText(messageDTO.getText());
        message.setChat(chatRepository.getById(messageDTO.getIdChat()));
        message.setIdSender(messageDTO.getIdSender());

        return message;

    }
    @Transactional
    public void save(Message message){
        log.info("Start method save with ", message);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        User user = userRepository.findByUsername(currentPrincipalName).get();

        message.setIdSender(user.getId());
        if (chatRepository.existsById(message.getChat().getId())){

            if (message.getChat().getUserSenderId() == message.getIdSender()){

                messageRepository.save(message);

            } else {
                NotFoundException notFoundException = new NotFoundException("not found available chat ");
                log.error(notFoundException.getMessage());
                throw notFoundException;

            }

        }else {

            NotFoundException notFoundException = new NotFoundException("not found  chat ");
            log.error(notFoundException.getMessage());
            throw notFoundException;

        }

        Message oppositeMessage = new Message();

        List<Chat> chatList = chatRepository.getChatByUsersId(message.getChat().getUserRecipientId(), message.getChat().getUserSenderId());

        for (Chat oppositeChat : chatList){

            oppositeMessage.setChat(oppositeChat);

        }

        oppositeMessage.setId(0);
        oppositeMessage.setText(message.getText());
        oppositeMessage.setIdSender(message.getIdSender());


        if (chatRepository.existsById(oppositeMessage.getChat().getId())){

            if (oppositeMessage.getChat().getUserRecipientId() == oppositeMessage.getIdSender()){

                messageRepository.save(oppositeMessage);

            }else {

                NotFoundException notFoundException = new NotFoundException("not found available chat ");
                log.error(notFoundException.getMessage());
                throw notFoundException;


            }

        }else {
            NotFoundException notFoundException = new NotFoundException("not found  chat ");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }
    }
    @Transactional
    public  void delete(Message message){

        log.info("Start method delete with ", message);

        if (message != null){

            messageRepository.delete(message);
        }else {
            NotFoundException notFoundException = new NotFoundException("message not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }



    }
    @Transactional
    public Message getById(long id){

        log.info("Start method getById with ", id);

        if(messageRepository.existsById(id)){
            return messageRepository.getById(id);
        }else {
            NotFoundException notFoundException = new NotFoundException("message not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }


    }

}
