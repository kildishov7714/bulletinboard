package ru.kildishov.servise;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kildishov.exception.NotFoundException;
import ru.kildishov.model.Advertisement;
import ru.kildishov.model.Comment;
import ru.kildishov.model.User;
import ru.kildishov.model.dto.AdvertisementDTO;
import ru.kildishov.model.dto.CommentDTO;
import ru.kildishov.model.dto.UserDTO;
import ru.kildishov.repository.*;

import java.util.ArrayList;
import java.util.List;
@Slf4j
@Service
public class UserServise {

    private final IAdvertisementRepository advertisementRepository;
    private final IChatRepository chatRepository;
    private final ICommetnRepository commetnRepository;
    private final IMessageRepository messageRepository;
    private final IUserRepository userRepository;
    private  final  AdvertisementServise advertisementServise;
    private final  CommentService commentService;

    @Autowired
    public UserServise(IAdvertisementRepository advertisementRepository, IChatRepository chatRepository, ICommetnRepository commetnRepository, IMessageRepository messageRepository, IUserRepository userRepository, AdvertisementServise advertisementServise, CommentService commentService) {

        this.advertisementRepository = advertisementRepository;
        this.chatRepository = chatRepository;
        this.commetnRepository = commetnRepository;
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
        this.advertisementServise = advertisementServise;
        this.commentService = commentService;
    }

    public UserDTO toDTO(User user){
        log.info("Start method toDTO with ", user);
        List<AdvertisementDTO> advertisementDTOList = new ArrayList<>();
        List<CommentDTO> commentDTOList = new ArrayList<>();

        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());

        if(user.getAdvertisementList() != null) {
            for (Advertisement advertisement : user.getAdvertisementList()) {

                AdvertisementDTO advertisementDTO = advertisementServise.toDTO(advertisement);
                advertisementDTOList.add(advertisementDTO);

            }

            userDTO.setAdvertisementDTOList(advertisementDTOList);
        }else {

            userDTO.setAdvertisementDTOList(null);
        }


        return  userDTO;

    }

    public User fromDTO(UserDTO userDTO){
        log.info("Start method toDTO with ", userDTO);
        List<Advertisement> advertisementList = new ArrayList<>();
        List<Comment> commentList = new ArrayList<>();

        User user = new User();

        user.setId(userDTO.getId());
        user.setUsername(userDTO.getUsername());

        if (userDTO.getAdvertisementDTOList() != null) {

            for (AdvertisementDTO advertisementDTO : userDTO.getAdvertisementDTOList()) {
                Advertisement advertisement = advertisementServise.fromDTO(advertisementDTO);
                advertisementList.add(advertisement);
            }
            user.setAdvertisementList(advertisementList);
        }else {
            user.setAdvertisementList(null);
        }

        return  user;
    }
    @Transactional
    public void save(User user){
        log.info("Start method toDTO with ", user);
        userRepository.save(user);

    }
    @Transactional
    public void delete(User user){

        log.info("Start method delete with ", user);
        userRepository.delete(user);


    }
    @Transactional
    public User getById(long id){

        log.info("Start method getById with ", id);

        if(userRepository.existsById(id)){
            return userRepository.getById(id);
        }else {
            NotFoundException notFoundException = new NotFoundException("User not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }

    }
    @Transactional
    public List<User> findAll(){
        log.info("Start method findAll with ");
        return userRepository.findAll();

    }
    @Transactional
    public User  changeUsername(long id, String username){

        if(userRepository.existsById(id)){
            User user = userRepository.getById(id);

            user.setUsername(username);

            userRepository.save(user);

            return user;

        }else {
            NotFoundException notFoundException = new NotFoundException("User not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }



    }
    @Transactional
    public User  changeBehavior(long id, int behavior){

        if(userRepository.existsById(id)) {

            User user = userRepository.getById(id);

            user.setBehavior(behavior);

            userRepository.save(user);

            return user;
        }else {
            NotFoundException notFoundException = new NotFoundException("User not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;

        }

    }


}
