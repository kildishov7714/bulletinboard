package ru.kildishov.servise;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kildishov.exception.NotFoundException;
import ru.kildishov.model.Comment;
import ru.kildishov.model.dto.CommentDTO;
import ru.kildishov.repository.*;

@Slf4j
@Service
public class CommentService {

    private final IAdvertisementRepository advertisementRepository;
    private final ICommetnRepository commetnRepository;
    private final IUserRepository userRepository;
    @Autowired
    public CommentService(IAdvertisementRepository advertisementRepository, IChatRepository chatRepository, ICommetnRepository commetnRepository, IMessageRepository messageRepository, IUserRepository userRepository) {
        this.advertisementRepository = advertisementRepository;
        this.commetnRepository = commetnRepository;
        this.userRepository = userRepository;
    }

    public CommentDTO toDTO(Comment comment){
        log.info("Start method toDTO with ",comment);

        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setIdUser(comment.getUser().getId());
        commentDTO.setText(comment.getText());
        commentDTO.setIdAdvertisement(comment.getAdvertisement().getId());
        return commentDTO;
    }
    @Transactional
    public Comment fromDTO(CommentDTO commentDTO){

        log.info("Start method fromDTO with ",commentDTO);
        Comment comment = new Comment();

        comment.setId(commentDTO.getId());
        comment.setText(commentDTO.getText());
        comment.setUser(userRepository.getById(commentDTO.getIdUser()));
        comment.setAdvertisement(advertisementRepository.getById(commentDTO.getIdAdvertisement()));

        return comment;

    }
    @Transactional
    public void save(Comment comment){

        log.info("Start method save with ",comment);

        if(advertisementRepository.existsById(comment.getAdvertisement().getId())){

            commetnRepository.save(comment);

        }else {
            NotFoundException notFoundException = new NotFoundException("advertisement doesn't exist");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }



    }
    @Transactional
    public void delete(Comment comment){

        log.info("Start method delete with ", comment);

        if (comment != null){

            commetnRepository.delete(comment);
        }else {
            NotFoundException notFoundException = new NotFoundException("comment not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }




    }
    @Transactional
    public Comment getById(long id){
        log.info("Start method getById with ", id);

        if(commetnRepository.existsById(id)){
            return commetnRepository.getById(id);
        }else {
            NotFoundException notFoundException = new NotFoundException("comment not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }



    }
}
