package ru.kildishov.servise;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kildishov.exception.NotFoundException;
import ru.kildishov.model.Advertisement;
import ru.kildishov.model.Chat;
import ru.kildishov.model.Message;
import ru.kildishov.model.User;
import ru.kildishov.model.dto.ChatDTO;
import ru.kildishov.model.dto.MessageDTO;
import ru.kildishov.repository.*;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ChatServise {

    private final IAdvertisementRepository advertisementRepository;
    private final IChatRepository chatRepository;

    private final IUserRepository userRepository;
    private final MessageServise messageServise;

    @Autowired
    public ChatServise(IAdvertisementRepository advertisementRepository, IChatRepository chatRepository, ICommetnRepository commetnRepository, IMessageRepository messageRepository, IUserRepository userRepository, MessageServise messageServise) {
        this.advertisementRepository = advertisementRepository;
        this.chatRepository = chatRepository;

        this.userRepository = userRepository;
        this.messageServise = messageServise;
    }

    public ChatDTO toDTO(Chat chat) {
        log.info("Start method toDTO  with", chat);

        List<MessageDTO> messageDTOList = new ArrayList<>();

        ChatDTO chatDTO = new ChatDTO();

        chatDTO.setId(chat.getId());
        chatDTO.setIdSender(chat.getUserSenderId());
        chatDTO.setIdRecipient(chat.getUserRecipientId());

        if (chat.getMessageList() != null) {

            for (Message message : chat.getMessageList()) {

                MessageDTO messageDTO = messageServise.toDTO(message);
                messageDTOList.add(messageDTO);

            }


            chatDTO.setMessageDTOList(messageDTOList);

        } else {
            chatDTO.setMessageDTOList(null);
        }


        return chatDTO;
    }

    public Chat fromDTO(ChatDTO chatDTO) {
        log.info("Start method fromDTO  with", chatDTO);
        List<Message> messageList = new ArrayList<>();

        Chat chat = new Chat();

        chat.setId(chatDTO.getId());
        chat.setUserSenderId(chatDTO.getIdSender());
        chat.setUserRecipientId(chatDTO.getIdRecipient());

        if (chatDTO.getMessageDTOList() != null) {

            for (MessageDTO messageDTO : chatDTO.getMessageDTOList()) {

                Message message = messageServise.fromDTO(messageDTO);
                messageList.add(message);

            }

            chat.setMessageList(messageList);

        } else {
            chat.setMessageList(null);
        }
        return chat;
    }

    public List<ChatDTO> listToDTO(List<Chat> chatList){
        log.info("Start method listToDTO  with", chatList);

        List<ChatDTO> chatDTOList = new ArrayList<>();

        for (Chat chat : chatList){

            ChatDTO chatDTO = toDTO(chat);
            chatDTOList.add(chatDTO);

        }

        return chatDTOList;


    }
    @Transactional
    public void save(Chat chat, Principal principal){
        log.info("Start method save  with", chat);


        //  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = principal.getName();

        User user = userRepository.findByUsername(currentPrincipalName).get();

        chat.setUserSenderId(user.getId());
        if(userRepository.existsById(user.getId()) ){

            if(userRepository.existsById(chat.getUserRecipientId()) ){

                if (user.getId() != chat.getUserRecipientId()){

                    if(getChatByUsersId(user.getId(),chat.getUserRecipientId()).isEmpty()){
                        chatRepository.save(chat);
                    }
                }

            }else {
                NotFoundException notFoundException = new NotFoundException("recipient user not found");
                log.error(notFoundException.getMessage());
                throw notFoundException;
            }

        }else {
            NotFoundException notFoundException = new NotFoundException("sender user not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;

        }

        Chat chatOpposite = new Chat();

        List<Message> messageList = new ArrayList<>();

        chatOpposite.setId(0);
        chatOpposite.setUserSenderId(chat.getUserRecipientId());
        chatOpposite.setUserRecipientId(user.getId());
        chatOpposite.setMessageList(messageList);

        if(userRepository.existsById(chatOpposite.getUserSenderId()) ){

            if(userRepository.existsById(chatOpposite.getUserRecipientId()) ){

                if (chatOpposite.getUserSenderId() != chatOpposite.getUserRecipientId()){

                    if(getChatByUsersId(chatOpposite.getUserSenderId(),chatOpposite.getUserRecipientId()).isEmpty()){

                        chatRepository.save(chatOpposite);

                    }

                }

            }else {

                NotFoundException notFoundException = new NotFoundException("recipient user not found");
                log.error(notFoundException.getMessage());
                throw notFoundException;

            }

        }else {

            NotFoundException notFoundException = new NotFoundException("sender user not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;


        }



    }
    @Transactional
    public  void delete(Chat chat){
        log.info("Start method delete with ", chat);

        if (chat != null){

            chatRepository.delete(chat);
        }else {
            NotFoundException notFoundException = new NotFoundException("Chat not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }



    }
    @Transactional
    public Chat getById(long id, Principal principal){

        log.info("Start method getById with ", id);

        Advertisement advertisement = advertisementRepository.getById(id);

        //  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = principal.getName();

        User user = userRepository.findByUsername(currentPrincipalName).get();

        if(chatRepository.getById(id).getUserSenderId() == user.getId()){

            return chatRepository.getById(id);

        }else {
            NotFoundException notFoundException = new NotFoundException("wrong id chat");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }


    }
    @Transactional
    public List<Chat> findAll(){
        log.info("Start method findAll  ");

        return chatRepository.findAll();

    }
    @Transactional
    public List<Chat> getChatByUsersId(long idSender, long idRecipient){

        log.info("Start method findAll with ", idSender, idRecipient);

        if(userRepository.existsById(idSender)){

            if (userRepository.existsById(idRecipient)){

                return chatRepository.getChatByUsersId(idSender, idRecipient);

            }else {
                NotFoundException notFoundException = new NotFoundException("wrong id recipient");
                log.error(notFoundException.getMessage());
                throw notFoundException;
            }

        }else {
            NotFoundException notFoundException = new NotFoundException("wrong id sender");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }



    }



}

