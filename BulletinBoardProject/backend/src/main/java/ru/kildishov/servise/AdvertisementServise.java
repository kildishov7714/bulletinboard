package ru.kildishov.servise;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.kildishov.exception.NotFoundException;
import ru.kildishov.model.Advertisement;
import ru.kildishov.model.Comment;
import ru.kildishov.model.User;
import ru.kildishov.model.dto.AdvertisementDTO;
import ru.kildishov.model.dto.CommentDTO;
import ru.kildishov.repository.*;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AdvertisementServise {

    private final IAdvertisementRepository advertisementRepository;

    private final IUserRepository userRepository;
    private final CommentService commentService;

    @Autowired
    public AdvertisementServise(IAdvertisementRepository advertisementRepository, IChatRepository chatRepository, ICommetnRepository commetnRepository, IMessageRepository messageRepository, IUserRepository userRepository, CommentService commentService){

        this.advertisementRepository = advertisementRepository;
        this.userRepository = userRepository;
        this.commentService = commentService;
    }

    public AdvertisementDTO toDTO(Advertisement advertisement){

        log.info("Method toDTO start with parametrs", advertisement);
        List<CommentDTO> commentDTOList = new ArrayList<>();

        AdvertisementDTO advertisementDTO = new AdvertisementDTO();

        advertisementDTO.setId(advertisement.getId());
        if (advertisement.getEnumCategoryAdvertisement() != null) {
            advertisementDTO.setEnumCategoryAdvertisement(advertisement.getEnumCategoryAdvertisement());
        }else {
            NotFoundException notFoundException = new NotFoundException("Category not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }

        advertisementDTO.setDescribtion(advertisement.getDescription());
        advertisementDTO.setIdUser(advertisement.getUser().getId());
        advertisementDTO.setEnumCategoryAdvertisement(advertisement.getEnumCategoryAdvertisement());

        if(advertisement.getCommentList() != null) {

            for (Comment comment : advertisement.getCommentList()) {

                CommentDTO commentDTO = commentService.toDTO(comment);
                commentDTOList.add(commentDTO);
            }

            advertisementDTO.setCommentDTOList(commentDTOList);
        } else {
            advertisementDTO.setCommentDTOList(null);
        }

        return advertisementDTO;
    }

    public  Advertisement fromDTO(AdvertisementDTO advertisementDTO){

        log.info("Method fromDTO start with parametrs", advertisementDTO);

        List<Comment> commentList = new ArrayList<>();

        Advertisement advertisement = new Advertisement();

        advertisement.setId(advertisementDTO.getId());
        advertisement.setUser(userRepository.getById(advertisementDTO.getIdUser()));
        advertisement.setDescription(advertisementDTO.getDescribtion());
        if (advertisementDTO.getEnumCategoryAdvertisement() != null){
            advertisement.setEnumCategoryAdvertisement(advertisementDTO.getEnumCategoryAdvertisement());
        }else {
            NotFoundException notFoundException = new NotFoundException("Category not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }

        if(advertisementDTO.getCommentDTOList() != null) {

            for (CommentDTO commentDTO : advertisementDTO.getCommentDTOList()) {

                Comment comment = commentService.fromDTO(commentDTO);
                commentList.add(comment);

            }
            advertisement.setCommentList(commentList);

        }else {
            advertisement.setCommentList(null);
        }
        return  advertisement;

    }
    @Transactional
    public List<AdvertisementDTO> listToDTO(List<Advertisement> advertisementList){

        log.info("Start method listToDTO start with parametrs", advertisementList);

        List<AdvertisementDTO> advertisementDTOList = new ArrayList<>();

        for (Advertisement advertisement : advertisementList){

            AdvertisementDTO advertisementDTO = toDTO(advertisement);
            advertisementDTOList.add(advertisementDTO);

        }

        return advertisementDTOList;


    }

    @Transactional
    public void save(Advertisement advertisement, Principal principal){

        log.info("Start method save start with parametrs", advertisement);

        // Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = principal.getName();
        User user = userRepository.findByUsername(currentPrincipalName).get();

        advertisement.setUser(user);
        advertisement.setSold(false);
        advertisement.setBoost(false);

        if (userRepository.existsById(advertisement.getUser().getId())){

            advertisementRepository.save(advertisement);

        }else {
            NotFoundException notFoundException = new NotFoundException("User doesn't exist");
            log.error(notFoundException.getMessage());
            throw notFoundException;

        }

    }
    @Transactional
    public  void delete(Advertisement advertisement){
        log.info("Start method delete with ", advertisement);

        if (advertisement != null){

            advertisementRepository.delete(advertisement);
        }else {
            NotFoundException notFoundException = new NotFoundException("Advertisement not found");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }
    }
    @Transactional
    public Advertisement getById(long id){

        log.info("Start method getById with id", id);
        return advertisementRepository.getById(id);
    }
    @Transactional
    public List<Advertisement> findAll(){

        log.info("Start method getById ");
        return advertisementRepository.findAll(Sort.by(Sort.Direction.DESC, "boost"));

    }
    @Transactional
    public List<Advertisement> sortByEnumCategoryAdvertisement(){

        log.info("Start method sortByEnumCategoryAdvertisement ");
        return advertisementRepository.sortByEnumCategoryAdvertisement();

    }
    @Transactional
    public Advertisement changeDescription(long id, String description, Principal principal){

        log.info("Start method changeDescription with", id, description);

        Advertisement advertisement = advertisementRepository.getById(id);

        // Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = principal.getName();

        User user = userRepository.findByUsername(currentPrincipalName).get();

        if(advertisement.getUser().getId() == user.getId()){

            advertisement.setDescription(description);

            advertisementRepository.save(advertisement);

            return advertisement;

        }{
            NotFoundException notFoundException = new NotFoundException("wrong id advertisement");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }
    }
    @Transactional
    public Advertisement changeBoost(long id, Boolean boost){

        log.info("Start method changeBoost with", id, boost);

        if(advertisementRepository.existsById(id) ){

            Advertisement advertisement = advertisementRepository.getById(id);

            advertisement.setBoost(boost);

            advertisementRepository.save(advertisement);

            return advertisement;

        }else {
            NotFoundException notFoundException = new NotFoundException("wrong id advertisement");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }

    }
    @Transactional
    public List<Advertisement> serchByEnumCategoryAdvertisement(String enumCategoryAdvertisement){

        log.info("Start method serchByEnumCategoryAdvertisement  with", enumCategoryAdvertisement);

        return advertisementRepository.serchByEnumCategoryAdvertisement(enumCategoryAdvertisement);

    }
    @Transactional
    public Advertisement changeSold(long id, Principal principal){

        log.info("Start method changeSold  with", id);

        Advertisement advertisement = advertisementRepository.getById(id);

        String currentPrincipalName = principal.getName();

        User user = userRepository.findByUsername(currentPrincipalName).get();

        if(advertisement.getUser().getId() == user.getId()){

            advertisement.setSold(true);

            advertisementRepository.save(advertisement);

            return advertisement;

        }else {

            NotFoundException notFoundException = new NotFoundException("wrong id advertisement");
            log.error(notFoundException.getMessage());
            throw notFoundException;
        }




    }
    @Transactional
    public List<Advertisement> getListByHistorySold(long id){

        log.info("Start method changeSold  with", id);

        if(userRepository.existsById(id)){
            return advertisementRepository.getListByHistorySold(id);
        }else{
            NotFoundException notFoundException = new NotFoundException("wrong id advertisement");
            log.error(notFoundException.getMessage());
            throw notFoundException;

        }

    }



}
