package ru.kildishov.model.enums;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
