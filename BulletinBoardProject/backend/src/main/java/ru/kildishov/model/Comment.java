package ru.kildishov.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( nullable = false)
    private  long id;
    @Column
    private  String text;
    @ManyToOne(fetch = FetchType.LAZY)
    private  User user;
    @ManyToOne(fetch = FetchType.LAZY)
    private Advertisement advertisement;
}
