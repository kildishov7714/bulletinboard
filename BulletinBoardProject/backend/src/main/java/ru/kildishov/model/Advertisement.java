package ru.kildishov.model;

import lombok.Getter;
import lombok.Setter;
import ru.kildishov.model.enums.EnumCategoryAdvertisement;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "advertisement")
public class Advertisement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private  long id;
    @Enumerated(EnumType.STRING)
    @Column
    private EnumCategoryAdvertisement enumCategoryAdvertisement;
    @Column
    private String description;
    @Column
    private Boolean boost;
    @Column
    private  Boolean sold;
    @ManyToOne
    private User user;
    @OneToMany(mappedBy = "advertisement", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
    private List<Comment> commentList = new ArrayList<>();

    public Advertisement(){
    }
}