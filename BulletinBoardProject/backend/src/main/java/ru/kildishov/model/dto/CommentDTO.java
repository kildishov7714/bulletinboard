package ru.kildishov.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentDTO {

    private long id;

    private long idUser;

    private String text;

    private long idAdvertisement;

}
