package ru.kildishov.model.dto;

import lombok.Getter;
import lombok.Setter;
import ru.kildishov.model.enums.EnumCategoryAdvertisement;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
public class AdvertisementDTO {

    private long id;

    private  long idUser;

    private EnumCategoryAdvertisement enumCategoryAdvertisement;

    private String describtion;

    private List<CommentDTO> commentDTOList = new ArrayList<>();

}
