package ru.kildishov.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageDTO {

    private long id;

    private String text;

    private long idSender;

    private long idChat;
}
