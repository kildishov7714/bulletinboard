package ru.kildishov.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ChatDTO {

    private long id;

    private long idSender;

    private  long idRecipient;

    private List<MessageDTO> messageDTOList;
}
