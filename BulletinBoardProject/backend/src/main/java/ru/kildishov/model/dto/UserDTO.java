package ru.kildishov.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserDTO {

    private long id;

    private String username;

    private List<AdvertisementDTO> advertisementDTOList;
}

