package ru.kildishov.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "message")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private  long id;
    @Column
    private String text;
    @Column
    private long idSender;
    @ManyToOne
    private Chat chat;

}
