package ru.kildishov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.kildishov.model.Chat;

import java.util.List;

@Repository
public interface IChatRepository extends JpaRepository<Chat, Long> {

    @Query(value = "select * from chats c where c.user_recipient_id = ?2 and c.user_sender_id = ?1",
            nativeQuery = true)
    List<Chat> getChatByUsersId(
            long idSender,  long  idRecipient);
}
