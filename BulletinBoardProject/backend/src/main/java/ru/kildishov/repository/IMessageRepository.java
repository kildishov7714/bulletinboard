package ru.kildishov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kildishov.model.Message;

@Repository
public interface IMessageRepository extends JpaRepository<Message, Long> {
}
