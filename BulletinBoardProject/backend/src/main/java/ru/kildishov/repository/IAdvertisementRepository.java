package ru.kildishov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.kildishov.model.Advertisement;

import java.util.List;

@Repository
public interface IAdvertisementRepository extends JpaRepository<Advertisement, Long> {

    @Query(value = "SELECT * FROM advertisement a ORDER BY  a.enum_category_advertisement",
            nativeQuery = true)
    List<Advertisement> sortByEnumCategoryAdvertisement();

    @Query(value = "select  * " +
            "from advertisement a  " +
            "left outer join users u on u.id  = a.user_id " +
            "where a.enum_category_advertisement = ?1 order by u.behavior DESC", nativeQuery = true)
    List<Advertisement> serchByEnumCategoryAdvertisement(String enumCategoryAdvertisement);

    @Query(value = "select * from advertisement a left outer join users u on u.id = a.user_id where u.id = ?1 and a.sold = true ", nativeQuery = true)
    List<Advertisement> getListByHistorySold(long id);


}
