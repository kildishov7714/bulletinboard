package ru.kildishov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kildishov.model.Comment;

@Repository
public interface ICommetnRepository extends JpaRepository<Comment, Long> {
}
