package ru.kildishov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "ru.kildishov")
public class Application {
    public static void main(String[] args){
        SpringApplication.run(Application.class, args);
    }
}
