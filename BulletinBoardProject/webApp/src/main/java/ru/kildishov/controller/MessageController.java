package ru.kildishov.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.kildishov.model.Message;
import ru.kildishov.model.dto.MessageDTO;
import ru.kildishov.servise.MessageServise;

import java.security.Principal;

@RestController
@RequestMapping(path ="/messages", headers = "Accept=application/json")
@CrossOrigin(origins = "*", maxAge = 3600)
public class MessageController {

    private MessageServise messageServise;
    @Autowired
    public MessageController ( MessageServise messageServise){

        this.messageServise = messageServise;

    }

    @PostMapping
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public MessageDTO save(@RequestBody MessageDTO messageDTO){

        Message message = messageServise.fromDTO(messageDTO);
        messageServise.save(message);
        return  messageServise.toDTO(message);

    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public MessageDTO show(@PathVariable long id){
        MessageDTO messageDTO = messageServise.toDTO(messageServise.getById(id));
        return  messageDTO;
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete( @PathVariable long id){

        messageServise.delete(messageServise.getById(id));

    }
}
