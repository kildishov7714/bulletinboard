package ru.kildishov.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.kildishov.model.Advertisement;
import ru.kildishov.model.dto.AdvertisementDTO;
import ru.kildishov.servise.AdvertisementServise;

import java.security.Principal;
import java.util.List;


@RestController
@RequestMapping(path ="/advertisements", headers = "Accept=application/json")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AdvertisementController {

    private  final AdvertisementServise advertisementServise;
    @Autowired
    public AdvertisementController(AdvertisementServise advertisementServise){

        this.advertisementServise = advertisementServise;
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ADMIN')")
    public AdvertisementDTO save(@RequestBody AdvertisementDTO advertisementDTO, Principal principal){

        Advertisement advertisement = advertisementServise.fromDTO(advertisementDTO);
        advertisementServise.save(advertisement, principal);
        return advertisementServise.toDTO(advertisement);

    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete( @PathVariable long id){

        advertisementServise.delete(advertisementServise.getById(id));

    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public AdvertisementDTO show(@PathVariable long id){
        AdvertisementDTO advertisementDTO = advertisementServise.toDTO(advertisementServise.getById(id));
        return  advertisementDTO;
    }


    @GetMapping
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<AdvertisementDTO> findAll() {

       return advertisementServise.listToDTO(advertisementServise.findAll());

    }

    @GetMapping("/sortCategory")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<AdvertisementDTO> sortByEnumCategoryAdvertisement(){

       return advertisementServise.listToDTO(advertisementServise.sortByEnumCategoryAdvertisement());

    }

    @PutMapping("/change/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public AdvertisementDTO changeDescription(@PathVariable long id , @RequestParam String description, Principal principal){

        principal.getName();
        AdvertisementDTO advertisementDTO = advertisementServise.toDTO(advertisementServise.changeDescription(id, description, principal));
        return advertisementDTO;
    }

    @PutMapping("/changeBoost/{id}")
    @PreAuthorize("hasRole('USER')")
    @ResponseStatus(HttpStatus.OK)
    public AdvertisementDTO changeBoost(@PathVariable long id, @RequestParam Boolean boost){

        AdvertisementDTO advertisementDTO = advertisementServise.toDTO( advertisementServise.changeBoost(id, boost));
        return advertisementDTO;

    }

    @GetMapping("/serchByCategory")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<AdvertisementDTO> serchByEnumCategoryAdvertisement(@RequestParam String enumCategoryAdvertisement){

        return advertisementServise.listToDTO(advertisementServise.serchByEnumCategoryAdvertisement(enumCategoryAdvertisement));

    }

    @GetMapping("/getListHistory/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<AdvertisementDTO> getListByHistorySold(@PathVariable long id){

        return  advertisementServise.listToDTO(advertisementServise.getListByHistorySold(id));

    }

    @PutMapping("/changeSold/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public AdvertisementDTO changeSold(@PathVariable long id, Principal principal){

        AdvertisementDTO advertisementDTO = advertisementServise.toDTO( advertisementServise.changeSold(id, principal));
        return advertisementDTO;

    }



}
