package ru.kildishov.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.kildishov.model.Comment;
import ru.kildishov.model.dto.CommentDTO;
import ru.kildishov.servise.CommentService;


@RestController
@RequestMapping(path ="/comments", headers = "Accept=application/json")
@CrossOrigin(origins = "*", maxAge = 3600)
public class CommentController {

    private final CommentService commentService;
    @Autowired
    public CommentController(CommentService commentService){

        this.commentService = commentService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public CommentDTO save(@RequestBody CommentDTO commentDTO){

       Comment comment = commentService.fromDTO(commentDTO);
       commentService.save(comment);
       return  commentService.toDTO(comment);

    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public CommentDTO show(@PathVariable long id){
        CommentDTO commentDTO = commentService.toDTO(commentService.getById(id));
        return  commentDTO;
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete( @PathVariable long id){

        commentService.delete(commentService.getById(id));

    }

}
