package ru.kildishov.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.kildishov.model.User;
import ru.kildishov.model.dto.UserDTO;
import ru.kildishov.servise.UserServise;

import java.util.List;

@RestController
@RequestMapping(path ="/users", headers = "Accept=application/json")
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {

    private final UserServise userServise;

    @Autowired
    public  UserController(UserServise userServise){

        this.userServise = userServise;
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO save(UserDTO userDTO){

        User user = userServise.fromDTO(userDTO);
        userServise.save(user);
        return userServise.toDTO(user);

    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete( @PathVariable long id){

        userServise.delete(userServise.getById(id));

    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public UserDTO show(@PathVariable long id){
        UserDTO userDTO = userServise.toDTO(userServise.getById(id));
        return  userDTO;
    }
    @PutMapping("/change/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO changeUsername(@PathVariable long id, @RequestParam String username){

        UserDTO userDTO = userServise.toDTO( userServise.changeUsername(id, username));
        return userDTO;

    }

    @PutMapping("/changeBehavior/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO changeBehavior(@PathVariable long id, @RequestParam int behavior){

        UserDTO userDTO = userServise.toDTO( userServise.changeBehavior(id, behavior));
        return userDTO;

    }

}
