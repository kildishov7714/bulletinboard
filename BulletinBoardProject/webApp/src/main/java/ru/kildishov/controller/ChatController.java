package ru.kildishov.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.kildishov.model.Chat;
import ru.kildishov.model.dto.ChatDTO;
import ru.kildishov.servise.ChatServise;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(path ="/chats", headers = "Accept=application/json")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ChatController {

    private ChatServise chatServise;
    @Autowired
    public ChatController(ChatServise chatServise){
        this.chatServise = chatServise;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ADMIN')")
    public ChatDTO save(@RequestBody ChatDTO chatDTO, Principal principal){

        Chat chat = chatServise.fromDTO(chatDTO);
        chatServise.save(chat, principal);
        return  chatServise.toDTO(chat);

    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ChatDTO show(@PathVariable long id, Principal principal){
        ChatDTO chatDTO = chatServise.toDTO(chatServise.getById(id, principal));
        return  chatDTO;
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete( @PathVariable long id, Principal principal){

        chatServise.delete(chatServise.getById(id,principal));

    }

    @GetMapping("/getChatByUsers")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<ChatDTO> getChatByUsersId(@RequestParam long idSender, @RequestParam long idRecipient){

        return  chatServise.listToDTO(chatServise.getChatByUsersId(idSender, idRecipient));

    }




}
