package ru.kildishov.restControllerAdvice;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.kildishov.exception.NotFoundException;

@RestControllerAdvice
public class CustomRestController {

    @ExceptionHandler(value = NotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public  ResponsMessage notFoundException(NotFoundException notFoundException){

        return new ResponsMessage(notFoundException.getMessage());

    }
}
