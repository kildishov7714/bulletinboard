package ru.kildishov.restControllerAdvice;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponsMessage {

    private String message;

    public ResponsMessage(String message){
        this.message = message;
    }


}
